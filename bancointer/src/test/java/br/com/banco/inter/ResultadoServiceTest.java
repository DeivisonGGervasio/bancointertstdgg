package br.com.banco.inter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import br.com.inter.serviceImpl.ResultadoServiceImpl;

@RunWith(JUnit4.class)
public class ResultadoServiceTest {
	
	@Test
	public void gerarConcatenacaoTest() {
		ResultadoServiceImpl server = new ResultadoServiceImpl();
		assertEquals("123", server.gerarConcatenacao(123L, 1L));
		assertEquals("123123", server.gerarConcatenacao(123L, 2L));
		assertEquals("123123123", server.gerarConcatenacao(123L, 3L));
		assertEquals("3", server.gerarConcatenacao(3L, 1L));
		assertEquals("33333", server.gerarConcatenacao(3L, 5L));
	}
	
	@Test
	public void calcularDigitoUnicoTest() {
		ResultadoServiceImpl server = new ResultadoServiceImpl();
		
		assertTrue(7L == server.calcularDigitoUnico("7"));
		assertTrue(3L == server.calcularDigitoUnico("12"));
		assertTrue(8L == server.calcularDigitoUnico("9875987598759875"));
	
	}
	
	@Test
	public void calcularValorResultadoTest() {
		
		ResultadoServiceImpl server = new ResultadoServiceImpl();
		
		assertTrue(8L == server.calcularValorResultado(9875L, 4L));
		
	}


}
