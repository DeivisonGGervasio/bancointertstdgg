package br.com.banco.inter;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import br.com.inter.cache.Cache;
import br.com.inter.entity.Resultado;

@RunWith(JUnit4.class)
public class CacheTest {
	
	
	/**
	 * Adiciona um item no cache e verifica se esta realmente cacheado. 
	 */
	@Test
	public void caso1() {
		
		Cache cache = new Cache();
		Resultado res = new Resultado(1L,2L,3L);
		cache.put(res);
	
		assertEquals(res, cache.get(res.getKey(), Resultado.class));
		assertNotEquals(new Resultado(2L,3L,4L), cache.get(res.getKey(), Resultado.class));
		
	}
	
	/**
	 * Adiciona um item no cache e verifica se a busca está funcionado paras os casos de itens não encontrados. 
	 */
	@Test
	public void caso2() {
		
		Cache cache = new Cache();
		Resultado res = new Resultado(1L,2L,3L);
		cache.put(res);
	
		assertNull(cache.get(Resultado.gerarKeyRes(3L,4L), Resultado.class));
	}
	
	/**
	 * Verifica se esta sendo colocados itens repetidos no cache.
	 */
	@Test
	public void caso3() {
		
		Cache cache = new Cache();
		Resultado res = new Resultado(1L,2L,3L);
		cache.put(res);
		cache.put(res);
		cache.put(res);
		cache.put(res);
	
		assertEquals(1, cache.size());
		
	}
	
	/**
	 * Verifica se o limite de 10 itens está sendo respeitado.
	 */
	@Test
	public void caso4() {
		
		Cache cache = new Cache();
		Resultado maisAntigo = new Resultado(1L,2L,3L);
		cache.put(maisAntigo);
		cache.put(new Resultado(2L,22L,13L));
		cache.put(new Resultado(3L,23L,23L));
		cache.put(new Resultado(4L,24L,33L));
		cache.put(new Resultado(5L,25L,43L));
		cache.put(new Resultado(6L,26L,53L));
		cache.put(new Resultado(7L,27L,63L));
		cache.put(new Resultado(8L,28L,73L));
		cache.put(new Resultado(9L,29L,83L));
		cache.put(new Resultado(10L,20L,93L));
		cache.put(new Resultado(11L,21L,3L));
		
		//não deve mais encontrar o item mais antigo.
		assertNull(cache.get(maisAntigo.getKey(), Resultado.class));
		
		//Verificando o limite.
		assertEquals(10, cache.size());
		
		assertNotNull(cache.get(Resultado.gerarKeyRes(21L,3L), Resultado.class));
		
	}


}
