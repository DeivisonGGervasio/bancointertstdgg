package br.com.inter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.inter.cache.Cacheble;

@Entity
@Table(name = "TB_RESULTADO")
public class Resultado implements Cacheble {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private Long numero;

	@Column
	private Long multiplicador;

	@Column
	private Long resultado;

	@Transient
	private Usuario usuario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public Long getMultiplicador() {
		return multiplicador;
	}

	public void setMultiplicador(Long multiplicador) {
		this.multiplicador = multiplicador;
	}

	public Long getResultado() {
		return resultado;
	}

	public void setResultado(Long resultado) {
		this.resultado = resultado;
	}

	public String getKey() {
		return gerarKeyRes(numero, multiplicador);
	}

	public static String gerarKeyRes(Long numero, Long multiplicador) {
		return String.format("%d_%d", numero, multiplicador);
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Resultado() {
		super();
	}

	public Resultado(Long id, Long numero, Long multiplicador) {
		super();
		this.id = id;
		this.numero = numero;
		this.multiplicador = multiplicador;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Resultado other = (Resultado) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
