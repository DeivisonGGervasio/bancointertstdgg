package br.com.inter.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.inter.dto.ResultadoDTO;
import br.com.inter.entity.Resultado;
import br.com.inter.entity.Usuario;
import br.com.inter.exception.BusinessException;
import br.com.inter.repository.IUsuarioRepository;

@Service
public class ResultadoConverter {

	@Autowired
	private IUsuarioRepository usuarioRepository;
	
	public ResultadoDTO entityToDTO(Resultado resultado) {
		
		ResultadoDTO resultadoDTO= new ResultadoDTO();
		
		resultadoDTO.setNumero(resultado.getNumero());
		resultadoDTO.setIdResultado(resultado.getId());
		resultadoDTO.setMultiplicador(resultado.getMultiplicador());
		resultadoDTO.setResultado(resultado.getResultado());
		resultadoDTO.setIdUsuario(resultado.getUsuario()!=null?resultado.getUsuario().getIdUsuario():null);
		
		return resultadoDTO;
	}

	public Resultado dtoToEntity(ResultadoDTO resultadoDTO) {
		
		Resultado resultado= new Resultado();
		
		resultado.setNumero(resultadoDTO.getNumero());
		resultado.setId(resultadoDTO.getIdResultado());
		resultado.setMultiplicador(resultadoDTO.getMultiplicador());
		resultado.setResultado(resultadoDTO.getResultado());
		
		if(resultadoDTO.getIdUsuario() != null) {
			resultado.setUsuario(buscarUsuarioPeloID(resultadoDTO.getIdUsuario()));
		}
		
		return resultado;
	}
	
	public Usuario buscarUsuarioPeloID(Long id) {
		//Criar exceçao da aplicaçao
		return usuarioRepository.findById(id)
				.orElseThrow(() -> new BusinessException("Erro ao encontrar Usuario",HttpStatus.NOT_FOUND));
	}
	
	public List<ResultadoDTO> converterListaEntityToDTO(List<Resultado> resultados) {
		return resultados.stream().map(res -> entityToDTO(res)).collect(Collectors.toList());
	}
	
	
	public List<Resultado> converterListaDTOToEntity(List<ResultadoDTO> resultados) {
		return resultados.stream().map(res -> dtoToEntity(res)).collect(Collectors.toList());
	}
}
