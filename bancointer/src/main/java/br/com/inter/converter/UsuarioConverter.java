package br.com.inter.converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.inter.dto.UsuarioDTO;
import br.com.inter.entity.Usuario;

@Service
public class UsuarioConverter {

	@Autowired
	private ResultadoConverter resultadoConverter;

	public UsuarioDTO entityToDTO(Usuario usuario) {

		UsuarioDTO usuarioDTO = new UsuarioDTO();

		usuarioDTO.setIdUsuarioDTO(usuario.getIdUsuario());
		usuarioDTO.setNome(usuario.getNome());
		usuarioDTO.setEmail(usuario.getEmail());

		if (CollectionUtils.isNotEmpty(usuario.getResultados())) {
			usuarioDTO.getResultadoDTOList()
					.addAll(resultadoConverter.converterListaEntityToDTO(usuario.getResultados()));
		}
		return usuarioDTO;
	}

	public Usuario dtoToEntity(UsuarioDTO usuarioDTO) {

		Usuario usuario = new Usuario();

		usuario.setIdUsuario(usuarioDTO.getIdUsuarioDTO());
		usuario.setNome(usuarioDTO.getNome());
		usuario.setEmail(usuarioDTO.getEmail());

		if (CollectionUtils.isNotEmpty(usuarioDTO.getResultadoDTOList())) {
			usuario.getResultados()
					.addAll(resultadoConverter.converterListaDTOToEntity(usuarioDTO.getResultadoDTOList()));
		}

		return usuario;
	}

	public List<UsuarioDTO> converterListaEntityToDTO(List<Usuario> listaUsuario) {
		List<UsuarioDTO> listaDTO = new ArrayList<>();

		for (Usuario usuario : listaUsuario) {
			listaDTO.add(entityToDTO(usuario));
		}

		return listaDTO;
	}
}
