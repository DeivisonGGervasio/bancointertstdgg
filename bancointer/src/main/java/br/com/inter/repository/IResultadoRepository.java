package br.com.inter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.inter.entity.Resultado;

@Repository
public interface IResultadoRepository extends JpaRepository<Resultado,Long>{
	
	 @Query("SELECT r FROM Usuario u join u.resultados r WHERE u.id = :id")
	 List<Resultado> buscarResultadoByUserioId(@Param("id") Long id);
	 
	 @Query("SELECT r FROM Resultado r where r.numero = :numero AND r.multiplicador = :multiplicador")
	 Resultado recuperarByNumeroMultiplicador(@Param("numero") Long numero,@Param("multiplicador") Long multiplicador);

}
