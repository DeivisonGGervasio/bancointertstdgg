package br.com.inter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.inter.entity.Usuario;

@Repository
public interface IUsuarioRepository extends JpaRepository<Usuario,Long>{

}
