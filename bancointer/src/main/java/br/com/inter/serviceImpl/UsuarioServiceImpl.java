package br.com.inter.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.inter.converter.UsuarioConverter;
import br.com.inter.dto.UsuarioDTO;
import br.com.inter.entity.Usuario;
import br.com.inter.exception.BusinessException;
import br.com.inter.repository.IUsuarioRepository;
import br.com.inter.service.IUsuarioService;

@Component
public class UsuarioServiceImpl implements IUsuarioService{
	
	@Autowired
	private IUsuarioRepository usuarioRepository;
	
	@Autowired
	private UsuarioConverter usuarioConverter;

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public UsuarioDTO atualizar(UsuarioDTO usuarioDTO) throws BusinessException {
		
		Usuario usuario = usuarioRepository.findById(usuarioDTO.getIdUsuarioDTO())
				.orElseThrow(() -> new BusinessException("Usuario não encontrado",HttpStatus.NOT_FOUND));
		
		
		atualizar(usuario,usuarioDTO);
		
		usuarioRepository.save(usuario);
		
		return usuarioConverter.entityToDTO(usuario);
	}


	private void atualizar(Usuario usuario, UsuarioDTO usuarioDTO) {
		usuario.setEmail(usuarioDTO.getEmail());
		usuario.setNome(usuarioDTO.getNome());
	}

	@Override
	public List<UsuarioDTO> buscarListaUsuario() throws BusinessException {

		List<Usuario> listaUsuario = usuarioRepository.findAll();

		if (listaUsuario.isEmpty()) {
			throw new BusinessException("Lista Vazia", HttpStatus.NOT_FOUND);
		}

		return usuarioConverter.converterListaEntityToDTO(listaUsuario);
	}

	@Override
	public UsuarioDTO buscarUsuarioById(Long idUsuario) throws BusinessException{
		Usuario usuario= usuarioRepository.findById(idUsuario)
				.orElseThrow(()->new BusinessException("Usuario não encontrado", HttpStatus.NOT_FOUND));
		
		return usuarioConverter.entityToDTO(usuario);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void excluirById(Long idUsuario) {
		usuarioRepository.deleteById(idUsuario);
	}


	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Usuario salvar(Usuario usuario) {
		return usuarioRepository.save(usuario);
	}

}
