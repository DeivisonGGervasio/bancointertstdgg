package br.com.inter.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.inter.cache.Cache;
import br.com.inter.converter.ResultadoConverter;
import br.com.inter.dto.ResultadoDTO;
import br.com.inter.entity.Resultado;
import br.com.inter.entity.Usuario;
import br.com.inter.exception.BusinessException;
import br.com.inter.repository.IResultadoRepository;
import br.com.inter.repository.IUsuarioRepository;
import br.com.inter.service.IResultadoService;

@Component
public class ResultadoServiceImpl implements IResultadoService {

	@Autowired
	private IResultadoRepository resultadoRepository;

	@Autowired
	private IUsuarioRepository usuarioRepository;
	
	@Autowired
	private ResultadoConverter resultadoConverter;
	
	@Autowired
	private Cache cache;


	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public ResultadoDTO calcularSalvarResultado(ResultadoDTO resultadoDTO){
		
		Resultado resultado = recuperarSalvarResultado(resultadoDTO);
		
		if(resultadoDTO.getIdUsuario() != null) {
			Usuario usuario= usuarioRepository.findById(resultadoDTO.getIdUsuario())
					.orElseThrow(() -> new BusinessException("Usuario não encontrado", HttpStatus.NOT_FOUND));	
			usuario.addResultado(resultado);
			usuarioRepository.save(usuario);
		}
		
		return resultadoConverter.entityToDTO(resultado);
	}

	private Resultado recuperarSalvarResultado(ResultadoDTO resultadoDTO) {
		//Tenta recuperar do cache.
		Resultado resultado = cache.get(Resultado.gerarKeyRes(resultadoDTO.getNumero(), resultadoDTO.getMultiplicador()), Resultado.class);
		
		if(resultado == null) {
			//Tenta buscar do banco.
			resultado = resultadoRepository.recuperarByNumeroMultiplicador(resultadoDTO.getNumero(), resultadoDTO.getMultiplicador());
		}
		
		if(resultado == null) {
			// Cria um novo registro de resultado.
			resultado = gerarNovoResultado(resultadoDTO);
		}
		cache.put(resultado);
		return resultado;
	}

	private Resultado gerarNovoResultado(ResultadoDTO resultadoDTO) {
		Resultado resultado = new Resultado();
		
		resultado.setMultiplicador(resultadoDTO.getMultiplicador());
		resultado.setNumero(resultadoDTO.getNumero());
		resultado.setResultado(calcularValorResultado(resultadoDTO.getNumero(), resultadoDTO.getMultiplicador()));

		resultado = resultadoRepository.save(resultado);
		return resultado;
	}

	public Long calcularValorResultado(Long numero, Long multiplicador) {
		//Gera a concatenação dos valores.
		String resultadoConcatenacao = gerarConcatenacao(numero,multiplicador);
		
		//Calcula o dígito único.
		return calcularDigitoUnico(resultadoConcatenacao);
	}

	public Long calcularDigitoUnico(String valor) {

		//Se o valor contem apenas um dígito retorna este digito.
		if(valor.length() == 1) {
			return Long.valueOf(valor);
		}
		//Transforama o valor em uma lista de caracteres.
		String novoValor = valor.chars().mapToObj(c -> (char) c).
		//Converte os caracteres para long.
		map(vl -> Long.valueOf(String.valueOf(vl))).
		//Realiza a soma dos valores
		reduce((vl1,vl2) ->vl1+vl2).get().toString() ;
		
		//Calcula novamente.
		return calcularDigitoUnico(novoValor);
	}

	public String gerarConcatenacao(Long numero, Long multiplicador) {
		StringBuilder resultadoSB = new StringBuilder();
		for (int i = 0; i < multiplicador; i++) {
			resultadoSB.append(numero);
		}
		return resultadoSB.toString();
	}

	@Override
	public List<ResultadoDTO> listaResultados() {
		List<Resultado> listaResultados = resultadoRepository.findAll();
		if (listaResultados.isEmpty()) {
			throw new BusinessException("Lista de resultados não encontrada.", HttpStatus.NOT_FOUND);
		}
		return resultadoConverter.converterListaEntityToDTO(listaResultados);
	}
	
	@Override
	public List<ResultadoDTO> buscarResultadoByUserioId(Long id) {
		List<Resultado> lista = resultadoRepository.buscarResultadoByUserioId(id);
		if (lista.isEmpty()) {
			throw new BusinessException("Nenhum usuário foi encontrado.", HttpStatus.NOT_FOUND);
		}
		return resultadoConverter.converterListaEntityToDTO(lista);
	}
	
	
}
