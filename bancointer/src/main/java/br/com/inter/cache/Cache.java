package br.com.inter.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * @author deivison.gervasio
 * 
 */
@Service
public class Cache {

	private Map<String, Entry> map = new HashMap<>();

	private List<Entry> list = new ArrayList<>();

	@SuppressWarnings("unchecked")
	public <T> T get(String key, Class<T> cl) {
		return map.containsKey(key) ? (T) map.get(key).getCacheble() : null;
	}

	public void put(Cacheble o) {

		// Se já está no cache retorna sem fazer nada.
		if (map.containsKey(o.getKey())) {
			return;
		}

		// Adiciona na estrutura.
		Entry entry = new Entry(o);
		map.put(o.getKey(), entry);
		list.add(entry);

		// Se atingiu o limite.
		if (list.size() > 10) {
			// Remove o mais antigo.
			Entry itemRemocao = list.remove(0);
			map.remove(itemRemocao.getKey());
		}
	}
	
	public int size() {
		return list.size();
	}

}

class Entry {

	private Cacheble cacheble;

	public String getKey() {
		return cacheble.getKey();
	}

	@Override
	public int hashCode() {
		return getKey().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Cacheble) {
			return getKey().equals(((Cacheble) obj).getKey());
		}
		return false;
	}

	public Entry(Cacheble cacheble) {
		super();
		this.cacheble = cacheble;
	}

	public Cacheble getCacheble() {
		return cacheble;
	}

}
