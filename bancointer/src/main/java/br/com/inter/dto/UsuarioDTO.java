package br.com.inter.dto;

import java.util.ArrayList;
import java.util.List;

public class UsuarioDTO {

	private Long idUsuarioDTO;

	private String nome;

	private String email;

	private List<ResultadoDTO> resultadoDTOList = new ArrayList<ResultadoDTO>();

	public UsuarioDTO() {
	}

	public Long getIdUsuarioDTO() {
		return idUsuarioDTO;
	}

	public void setIdUsuarioDTO(Long idUsuarioDTO) {
		this.idUsuarioDTO = idUsuarioDTO;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<ResultadoDTO> getResultadoDTOList() {
		return resultadoDTOList;
	}

	public void setResultadoDTOList(List<ResultadoDTO> resultadoDTOList) {
		this.resultadoDTOList = resultadoDTOList;
	}

	
	


}
