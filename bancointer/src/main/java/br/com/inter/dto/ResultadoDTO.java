package br.com.inter.dto;

public class ResultadoDTO {

	private Long idResultado;
	
	private Long numero;
	
	private Long multiplicador;
	
	private Long resultado;
	
	private Long idUsuario;
	
	public ResultadoDTO() {}

	public ResultadoDTO(Long idResultado, Long numero, Long multiplicador, Long resultado, Long idUsuario) {
		super();
		this.idResultado = idResultado;
		this.numero = numero;
		this.multiplicador = multiplicador;
		this.resultado = resultado;
		this.idUsuario = idUsuario;
	}

	public Long getIdResultado() {
		return idResultado;
	}

	public void setIdResultado(Long idResultado) {
		this.idResultado = idResultado;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public Long getMultiplicador() {
		return multiplicador;
	}

	public void setMultiplicador(Long multiplicador) {
		this.multiplicador = multiplicador;
	}

	public Long getResultado() {
		return resultado;
	}

	public void setResultado(Long resultado) {
		this.resultado = resultado;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

}
