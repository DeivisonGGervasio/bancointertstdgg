package br.com.inter.service;

import java.util.List;

import br.com.inter.dto.ResultadoDTO;
import br.com.inter.exception.BusinessException;


public interface IResultadoService {

	public ResultadoDTO calcularSalvarResultado(ResultadoDTO resultadoDTO) throws BusinessException;
	
	public List<ResultadoDTO>listaResultados();
	
	public List<ResultadoDTO> buscarResultadoByUserioId(Long id);
}