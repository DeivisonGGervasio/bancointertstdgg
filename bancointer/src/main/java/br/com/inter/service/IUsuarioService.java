package br.com.inter.service;

import java.util.List;

import br.com.inter.dto.UsuarioDTO;
import br.com.inter.entity.Usuario;
import br.com.inter.exception.BusinessException;


public interface IUsuarioService {

	/**
	 * 
	 * @param usuarioDTO
	 * @return
	 * @throws BusinessException
	 */
	public UsuarioDTO atualizar(UsuarioDTO usuarioDTO) throws BusinessException;
	
	/**
	 * 
	 * @return
	 */
	public List<UsuarioDTO> buscarListaUsuario()throws BusinessException;
	
	/**
	 * 
	 * @param idUsuario
	 * @return
	 */
	public UsuarioDTO buscarUsuarioById(Long idUsuario);
	
	/**
	 * 
	 * @param idUsuario
	 */
	public void excluirById(Long idUsuario);

	/**
	 * @param usuario
	 * @return
	 */
	public Usuario salvar(Usuario usuario);
}