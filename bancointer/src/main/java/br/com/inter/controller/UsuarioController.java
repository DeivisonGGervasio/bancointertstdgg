package br.com.inter.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.inter.converter.UsuarioConverter;
import br.com.inter.dto.UsuarioDTO;
import br.com.inter.entity.Usuario;
import br.com.inter.exception.BusinessException;
import br.com.inter.service.IUsuarioService;

@RestController
@RequestMapping(value="/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioConverter usuarioConverter;

	@Autowired
	private IUsuarioService usuarioService;
	

	@PostMapping("/salvar")
	public ResponseEntity<UsuarioDTO> salvar(@RequestBody UsuarioDTO usuarioDTO) {
		Usuario usuario = usuarioConverter.dtoToEntity(usuarioDTO);
		usuario = usuarioService.salvar(usuario);
		UsuarioDTO usuarioSalvo = usuarioConverter.entityToDTO(usuario);
		return ResponseEntity.status(HttpStatus.CREATED).body(usuarioSalvo);
	}

	@PutMapping("atualizar/{id}")
	public ResponseEntity<UsuarioDTO> atualizar(@PathVariable Long id, @Valid @RequestBody UsuarioDTO usuarioDTO) {
		try {
			UsuarioDTO usuario = usuarioService.atualizar(usuarioDTO);
			return ResponseEntity.status(HttpStatus.OK).body(usuario);
		} catch (BusinessException e) {
			return ResponseEntity.status(e.getStatus()).build();
		}
	}

	@DeleteMapping("excluir/{id}")
	public ResponseEntity<?> excluir(@PathVariable Long id) {
		try {
			usuarioService.excluirById(id);
			return ResponseEntity.status(HttpStatus.OK).build();
		} catch (BusinessException e) {

			return ResponseEntity.status(e.getStatus()).build();
		}

	}

	@GetMapping("/buscarusuario/{id}")
	public ResponseEntity<UsuarioDTO> buscaPeloCodigo(@PathVariable Long id) {
		
		try {
			
			UsuarioDTO usuarioDTO=usuarioService.buscarUsuarioById(id);
			
			return ResponseEntity.status(HttpStatus.OK).body(usuarioDTO);
			
		}catch (BusinessException e) {
			
			return ResponseEntity.status(e.getStatus()).build();
		}
	}

	@GetMapping("/listausuario")
	public ResponseEntity<List<UsuarioDTO>> buscarListaUsuario() {

		try {
			List<UsuarioDTO> listaUsuarioDTO = usuarioService.buscarListaUsuario();

			return ResponseEntity.status(HttpStatus.OK).body(listaUsuarioDTO);
		} catch (BusinessException e) {

			return ResponseEntity.status(e.getStatus()).build();
		}
	}
}
