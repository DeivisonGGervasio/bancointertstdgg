package br.com.inter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.inter.dto.ResultadoDTO;
import br.com.inter.service.IResultadoService;

@RestController
@RequestMapping(value = "/resultado")
public class ResultadoController {

	@Autowired
	private IResultadoService resultadoService;

	@PostMapping("/calcularesultado")
	public ResponseEntity<ResultadoDTO> calcularResultado(@RequestBody ResultadoDTO resultadoDTO) {
		ResultadoDTO resultadoCalculado = resultadoService.calcularSalvarResultado(resultadoDTO);
		return ResponseEntity.status(HttpStatus.OK).body(resultadoCalculado);
	}
	
	@GetMapping("/buscarresultadousuario/{id}")
	public ResponseEntity<List<ResultadoDTO>> listaResultadoByUsuarioId(@PathVariable Long id) {
		List<ResultadoDTO> listaResultado = resultadoService.buscarResultadoByUserioId(id);
		return ResponseEntity.status(HttpStatus.OK).body(listaResultado);
	}

	@GetMapping("/buscartodosresultados")
	public ResponseEntity<List<ResultadoDTO>> buscarTodosResultados() {
		List<ResultadoDTO> listaResultados = resultadoService.listaResultados();
		return ResponseEntity.status(HttpStatus.OK).body(listaResultados);
	}
}
