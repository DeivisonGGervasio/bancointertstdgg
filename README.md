# Teste de conhecimento do banco Inter

**Este projeto foi criado utilizando o Spring Boot. **

	

O que foi implementado:
	

1. CRUD de manutenção do usuário.
2. Serviço para realizar o cálculo de dígito único.
3. Mecanismo de cache simples.
4. Testes unitários de funcionalidades que não depende de banco de dados.
5. Banco em memória. 
	1. H2 http://localhost:8080/h2 com usuario=sa  senha=""
	

Versão da jvm: java 8.

Repositório:
https://DeivisonGGervasio@bitbucket.org/DeivisonGGervasio/bancointertstdgg.git

Execução:
Para rodar a aplicação basta executar o método main da classe BancointerApplication.

Obrigado pela oportunidade!!
